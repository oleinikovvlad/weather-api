window.onload = function () {
    create_ob_tile();

    var scanner = document.getElementById('scanner');
    var input = document.getElementById('scan');

    scanner.addEventListener('click', ()=>{
        if (input.value) search();
    })
};

var Tile = {
    ObTile: {},
    ObMDetails: {},
    ObMRemove: {},
    StartPointX: -1,
    StartPointY: -1,
    TileWidth: 0,
    Drag: 0,
    StartDrag: 0,
};
var mousedownID = -1;
var elementID = 0;
 
function create_ob_tile() {
    Tile.ObTile = {};
    Tile.ObMDetails = {};
    Tile.ObMRemove = {};
    let el_id;
    
    var mian_tile = document.querySelectorAll('.tile');
    mian_tile.forEach(element => {
        el_id = element.id.replace("tile-", '');

        Tile.ObTile[el_id]      = element;
        Tile.ObMDetails[el_id]  = document.getElementById("m-details-" + el_id);
        Tile.ObMRemove[el_id]   = document.getElementById("m-remove-" + el_id);
    });

    if (el_id){
        document.getElementById('help-message').classList.add('d-hidden');
    } else {
        document.getElementById('help-message').classList.remove('d-hidden');
    }

    for (element in Tile.ObTile) {
        block = Tile.ObTile[element];
        if ('ontouchstart' in window) {
            block.addEventListener("touchstart", handle_start, {passive: false});
            block.addEventListener("touchend", handle_end, false);
        } else {
            block.addEventListener("mousedown", mousedown, element);
            block.addEventListener("mouseup", mouseup);
            block.addEventListener("mouseleave", mouseup);
        }
    }
}
/**
 * function touch move
 */
function handle_start(e){
    Tile.StartPointX = e.touches[0].clientX;
    Tile.StartPointY = e.touches[0].clientY;
    el_id = parse_id(e);

    scrollX = window.scrollX;
    scrollY = window.scrollY;
    Tile.ObTile[el_id].addEventListener("touchmove", check_move_handle, {passive: false});
}
function check_move_handle(e){
    let moveX = e.touches[0].clientX;
    let moveY = e.touches[0].clientY;

    moveX = Math.abs(moveX-Tile.StartPointX)
    moveY = Math.abs(moveY-Tile.StartPointY)
    el_id = parse_id(e);
    if ((moveX+1)/(moveY+1) > 2){
        Tile.ObTile[el_id].removeEventListener("touchmove", check_move_handle, false);
        Tile.ObTile[el_id].addEventListener("touchmove", handle_move, {passive: false});
    } else {
        Tile.ObTile[el_id].removeEventListener("touchmove", check_move_handle, false);
    }
}
function handle_end(e){
    Tile.StartPointX = -1;
    Tile.StartPointY = -1;
    document.body.style.overflow = "auto";
    el_id = parse_id(e);
    Tile.ObTile[el_id].removeEventListener("touchmove", handle_move, false);
    tile_closer(e);
}
function handle_move(e){
    document.body.style.overflow = "hidden";
    mouse_move_x(e.touches[0]);
}
/**
 * function mouse move
 */
function mousedown(element) {
    if (mousedownID == -1) {
        mousedownID = setInterval(while_mouse_down, 100, element);
    }
}
function mouseup(e) {
    if (mousedownID != -1) {
        clearInterval(mousedownID);
        mousedownID = -1;
        el_id = parse_id(e);
        Tile.ObTile[el_id].removeEventListener('mousemove', mouse_move_x, e);
        tile_closer(e);
        setTimeout(() => {
            Tile.StartPointX = -1;
        }, 100);
    }
}
function while_mouse_down(element) {
    el_id = parse_id(element);

    Tile.StartPointX = element.clientX;
    Tile.ObTile[el_id].addEventListener('mousemove', mouse_move_x, element);
}
/**
 * function handling block movement
 */
function mouse_move_x (e) {
    if (Tile.TileWidth == 0){
        if(e.target.offsetParent.id.match('container')) {
            Tile.TileWidth   = e.target.offsetWidth;
        } 
        else {    
            Tile.TileWidth   = e.target.offsetParent.offsetWidth;
        }    
    } 
    el_id = parse_id(e);
    
    let block = Tile.ObTile[el_id];
    let details = Tile.ObMDetails[el_id];
    let remove = Tile.ObMRemove[el_id];
    let startDrag = Tile.StartDrag;
    let width = Tile.TileWidth;

    if ((Tile.StartPointX > -1) && (Tile.StartPointX != e.clientX)) {
        moveX = Tile.StartPointX - e.clientX
        moveX = moveX * (-1);
        switch (Tile.Drag) {
            case 0:
                if (moveX>0) Tile.Drag = 1;
                if (moveX<0) Tile.Drag = -1; 
                Tile.StartDrag = block.style.left.replace('px','')*1;
                [block, details, remove].forEach(element => {
                    element.classList.remove('animation');
                })
                break;
            case 1:
                block.style.left = startDrag + moveX+'px';
                if (startDrag == 0){
                    if (moveX>0) {
                        if (moveX < 99){
                            details.style.width = '100px';
                            details.style.left = -100+moveX+'px';
                        } else {
                            moveX = moveX-100;
                            details.style.left = 0+'px';
                            details.style.width = 100+moveX+'px';
                        }
                    }
                    if (moveX<0) {
                        block.style.width = width - moveX+'px';
                    }    
                } 
                if (startDrag > 0){
                    details.style.width = startDrag + moveX+'px';
                    if (moveX>0){
                        details.style.left = 0+'px';
                    } else {
                        if ((startDrag+moveX)>=0) {
                            details.style.width = startDrag +'px';
                            details.style.left = moveX+'px';    
                        } else {
                            block.style.width = width -(startDrag+moveX) +'px';
                        }
                    }
                }
                if (startDrag < 0){
                    if (moveX > 0){
                        if ((startDrag +moveX)>=0){
                            remove.style.right = '-100px';
                            block.style.left = '0px';
                            block.style.width = width +(startDrag+moveX)+'px' 
                        } else {
                            remove.style.right = -moveX+'px';
                            remove.style.width = -startDrag+'px'    
                        }
                    } else {
                        remove.style.right = '0px'
                        remove.style.width = -startDrag-moveX+'px';
                    }
                }
                break;
            case -1:
                block.style.left = startDrag + moveX+'px';
                if (startDrag == 0){
                    if (moveX<0) {
                        if (moveX > -99){
                            remove.style.width = '100px'
                            remove.style.right = -100-moveX+'px';
                        } else {
                            remove.style.right = 0+'px';
                            remove.style.width = -moveX+'px';
                        }
                    }
                    if (moveX>0) {
                        block.style.left = '0px';
                        block.style.width = width + moveX+'px';
                    }    
                }
                if (startDrag < 0){
                    remove.style.width = -startDrag - moveX+'px';
                    if (moveX<0){
                        remove.style.right = '0px';
                    } else {
                        if ((startDrag+moveX)<=0) {
                            remove.style.width = -startDrag +'px';
                            remove.style.right = -moveX+'px';
                        } else {
                            block.style.left = '0px';
                            block.style.width = width +(startDrag+moveX) +'px';
                        }
                    }
                }
                if (startDrag > 0){
                    details.style.width = startDrag + moveX+'px';
                    if (moveX<0){
                        if ((startDrag+moveX)>0){
                            details.style.width = startDrag +'px';
                            details.style.left = moveX+'px';
                            block.style.width = width +'px';
                        } else {
                            block.style.width = width - (startDrag+moveX) + 'px';
                        }
                    } else {
                        remove.style.right = '0px';
                    }
                }

                break;
        }
    }
}
/**
 * function processing the position of blocks after stopping movement 
 */
function tile_closer () {
    let block       = Tile.ObTile[el_id];
    let details     = Tile.ObMDetails[el_id];
    let remove      = Tile.ObMRemove[el_id];
    let width       = Tile.TileWidth;

    let positionBlock   = block.style.left.replace('px','')*1;
    let positionRemove  = remove.style.right.replace('px','');

    if (positionRemove == '') {positionRemove = '-100' }
    else {positionRemove = positionRemove*1}

    add_animation(block, details, remove);

    switch (Tile.Drag) {
        case 0:
            clear_style();

            break;    
        default:
            if (Tile.Drag > 0){
                if (positionBlock < 0){
                    check_menu(remove, false);
                }
                if (positionBlock > 0) {
                    check_menu(details, true);
                } 
                if (positionBlock == 0){
                    clear_style();
                }
            }
            if (Tile.Drag < 0){
                if (positionBlock > 0) {
                    check_menu(details, true);
                }
                if (positionBlock < 0) {
                    if (positionRemove == -100){
                        clear_style();
                    }
                    if (positionRemove > -100){
                        check_menu(remove, false);
                    }
                }
                if (positionBlock == 0) {
                    clear_style();
                }
            }
            break;
    }
    
    Tile.Drag = 0;
    Tile.StartDrag = 0;

    function clear_style(){
        details.removeEventListener('click', ()=>{})
        remove.removeEventListener('click', ()=>{})

        if(width == 0) block.style.width = '100%';
        else block.style.width = width+'px';

        block.style.left = '0px';
        details.style.left = '-100px';
        details.style.width = '100px';
        remove.style.right = '-100px';
        remove.style.width = '100px';
    }
    function check_menu(element, bool){
        let percent = Math.abs(positionBlock)*100/width;                   
        if ((10 < percent)&&( percent < 60)){
            element.style.width = '100px';
            if (bool){
                element.style.left = '0px';
                block.style.left = '100px';
                element.addEventListener('click', ()=>{
                    alert('подробный прогноз');
                })
            } else {
                element.style.right = '0px';
                block.style.left = '-100px';
                element.addEventListener('click', ()=>{
                    delet_tile(el_id);
                })
            }
        } else {
            if (percent >= 60){
                element.style.width = '100%';
                if (bool) {
                    block.style.left = '100%';
                    setTimeout(() => {
                        alert('подробный прогноз');
                    }, 1250);
                } else {
                    block.style.left = '-100%';
                    setTimeout(() => {
                        delet_tile(el_id);
                    }, 1250);
                }
            } 
            if (10 > percent){
                clear_style();
            }    
        }
    }
}

function add_animation (block, details, remove){
    block.classList.add('animation');
    details.classList.add('animation');
    remove.classList.add('animation');
    if ('ontouchstart' in window) {
        block.removeEventListener("touchstart", handle_start, {passive: false});
    } else {
        block.removeEventListener("mousedown", mousedown, element);
    }
    setTimeout(() => {
        block.classList.remove('animation');
        details.classList.remove('animation');
        remove.classList.remove('animation');
        if ('ontouchstart' in window) {
            block.addEventListener("touchstart", handle_start, {passive: false});
        } else {
            block.addEventListener("mousedown", mousedown, element);
        }
    }, 900);
}
function delet_tile (el_id){
    document.getElementById('container-'+el_id).remove();
    create_ob_tile();
}

function parse_id (e){
    el_id = e.target.id;
    if (el_id == '') {
        el_id = e.target.offsetParent.id;
    }
    el_id = el_id.replace("tile-", '');
    return el_id;
}

/**
 * city ​​search and output 
 */
function search() {
    let scan = document.getElementById("scan").value;
    if (scan !== '') {
        searchWeather(scan);
        document.getElementById("scan").value = '';
        document.getElementById("search-animation").classList.add('d-block');
        document.activeElement.blur();
    }
}
function searchWeather(name) {
    var data = {
        q : name,
        APPID : '5d7e95931f227c4ed8c506b9473eb53e',
    };
    $.ajax({
        url: 'http://api.openweathermap.org/data/2.5/weather',
        type: "GET",
        data: data,
        success: function (response) {
            /**
             * Вывод объекта при успешном запросе
             */
            console.log(response);

            let name = response.name;
            let temp = response.main.temp;
            let description = response.weather[0].description;
            let id = response.id;

            let html;
            html = '<div class="container" id="container-'+id+'"> ';
            html += '   <div class="menu details" id="m-details-'+id+'"> Подробнее </div>';
            html += '   <div class="tile" id="tile-'+id+'">'
            html += '       <div><h2>'+name+'</h2></div>'
            html += '       <div><span>'+temp+'</span><span> K</span></div>'
            html += '       <div><span>'+description+'</span></div>'
            html += '   </div>'
            html += '   <div class="menu remove" id="m-remove-'+id+'"> Удалить </div>'
            html += '</div>'    
                    
            document.getElementById('main').insertAdjacentHTML("afterbegin", html);
            document.getElementById("search-animation").classList.remove('d-block');
            create_ob_tile();
        },
        error: function (response) {
            /**
             *  status 0: нет возможности отправить сервис;
             * Вывод ошибок:
             *   API calls return an error 401: Ошибка с ключом (не правильный запрос/неправельный ключ)
             *   API calls return an error 404: Ошибка с поиском (не правильно указан город)
             *   API calls return an error 429: Ошибка с количеством запросов (больше 60 в минуту)
             */

            let status = response.status;
            let statusText = response.statusText;
            let message;
            try { if (response.responseJSON.message){
                    message = response.responseJSON.message;
                }
            } catch {}
            
            
            let text = {
                status: status,
                header: statusText,
                message: ': '+message
            }
            switch (status) {
                case 401:
                case 404:
                case 429:
                    construct_message(text);
                    break;
                case 0:
                default:
                    text.message = ': no opportunity to send a request';
                    construct_message(text);
                    break;
            }
            document.getElementById("search-animation").classList.remove('d-block');
        }
    });    
}
function construct_message (text){
    /* # main #*/
    let main = document.createElement('div');
    main.className = "error";
    /*# button pop-up #*/
    let popup = document.createElement('div');
    popup.className = "pop-up";
    /*# button exit #*/
    let buttonExit = document.createElement('span');
    buttonExit.className = "pop-exit";
    buttonExit.innerText ='X';
    buttonExit.id = "pop-exit";
    /*# header text #*/
    let textHeader = document.createElement('span');
    textHeader.className = "error-header";
    textHeader.innerText ='Eror: '+text.status;
    /*# message text #*/
    let textMessage = document.createElement('span');
    textMessage.className = "error-message";
    textMessage.innerText = text.header+text.message;

    popup.append(buttonExit,textHeader,textMessage);
    main.append(popup);

    document.getElementById('main').prepend(main);

    setTimeout(() => {
        main.style.opacity = '1';
    }, 500);
    
    buttonExit.addEventListener("click", () =>{
        main.style.opacity = '0';
        setTimeout(() => {
            buttonExit.removeEventListener('click', ()=>{});
            main.remove();
        }, 1500);    
    } );
}
